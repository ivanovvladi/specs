Pod::Spec.new do |spec|
  spec.name           = "SourceryTypes"
  spec.version        = "0.1.0"
  spec.summary        = "Sourcery Types"
  
  spec.description    = <<-DESC
    Sourcery types needed for applying code auto-generation
  DESC

  spec.homepage       = "https://gitlab.com/ivanovvladi"
  spec.license        = { :type => "Apache License Version 2.0", :file => "LICENSE" }
  spec.author         = { "Vladislav Ivanov" => "vladislav.ivanov.v@gmail.com" }
  spec.platform       = :ios, "10.0"
  spec.source         = { :git => "https://gitlab.com/ivanovvladi/SourceryTypes.git", :tag => "#{spec.version}" }
  spec.source_files   = "SourceryTypes/**/*.{swift}"
  spec.swift_versions = "5.0"

end