Pod::Spec.new do |spec|
  spec.name           = "Networking"
  spec.version        = "0.1.1"
  spec.summary        = "Networking DSL"
  
  spec.description    = <<-DESC
    A simple networking DSL
  DESC

  spec.homepage       = "https://gitlab.com/ivanovvladi"
  spec.license        = { :type => "Apache License Version 2.0", :file => "LICENSE" }
  spec.author         = { "Vladislav Ivanov" => "vladislav.ivanov.v@gmail.com" }
  spec.platform       = :ios, "10.0"
  spec.source         = { :git => "https://gitlab.com/ivanovvladi/Networking.git", :tag => "#{spec.version}" }
  spec.source_files   = "Networking/**/*.{swift}"
  spec.swift_versions = "5.0"

end