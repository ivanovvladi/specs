Pod::Spec.new do |spec|
  spec.name           = "StyleableUI"
  spec.version        = "0.1.2"
  spec.summary        = "Helpers to style UI"
  
  spec.description    = <<-DESC
    Simple helpers to style UI
  DESC

  spec.homepage       = "https://gitlab.com/ivanovvladi"
  spec.license        = { :type => "Apache License Version 2.0", :file => "LICENSE" }
  spec.author         = { "Vladislav Ivanov" => "vladislav.ivanov.v@gmail.com" }
  spec.platform       = :ios, "10.0"
  spec.source         = { :git => "https://gitlab.com/ivanovvladi/StyleableUI.git", :tag => "#{spec.version}" }
  spec.source_files   = "StyleableUI/**/*.{swift}"
  spec.swift_versions = "5.0"
  spec.dependency       "LayoutDSL", "#{spec.version}"

end