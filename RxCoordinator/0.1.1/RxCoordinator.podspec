Pod::Spec.new do |spec|
  spec.name           = "RxCoordinator"
  spec.version        = "0.1.1"
  spec.summary        = "Rx Coordinator"
  
  spec.description    = <<-DESC
    A simple coordinator based on the RxSwift
  DESC

  spec.homepage       = "https://gitlab.com/ivanovvladi"
  spec.license        = { :type => "Apache License Version 2.0", :file => "LICENSE" }
  spec.author         = { "Vladislav Ivanov" => "vladislav.ivanov.v@gmail.com" }
  spec.platform       = :ios, "10.0"
  spec.source         = { :git => "https://gitlab.com/ivanovvladi/Coordinator.git", :tag => "#{spec.version}" }
  spec.source_files   = "RxCoordinator/**/*.{swift}"
  spec.swift_versions = "5.0"
  spec.dependency       "RxSwift", "5.1.1"

end